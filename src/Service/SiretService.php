<?php

namespace Listeur\SiretValidator\Service;

class SiretService
{
    const LENGTH = 14;

    /**
     * @param $siret
     * @return bool
     */
    public static function isValidSiret($siret)
    {
        if (!is_numeric($siret)) {
            return false;
        }

        if (strlen($siret) != self::LENGTH) {
            return false;
        }

        $sum = 0;
        for ($i = 0; $i < self::LENGTH; ++$i) {
            $indice = (self::LENGTH - $i);
            $tmp = (2 - ($indice % 2)) * $siret[$i];
            if ($tmp >= 10) {
                $tmp -= 9;
            }
            $sum += $tmp;
        }

        return ($sum % 10) == 0;
    }
}
