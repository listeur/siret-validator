<?php

namespace Listeur\SiretValidator\Constraint;

use Symfony\Component\Validator\Constraint;

class Siret extends Constraint
{
    public $message = 'Siret invalide.';
}
