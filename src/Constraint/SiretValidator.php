<?php

namespace Listeur\SiretValidator\Constraint;

use Listeur\SiretValidator\Service\SiretService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class SiretValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Siret) {
            throw new UnexpectedTypeException($constraint, Siret::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_numeric($value) && !is_string($value)) {
            throw new UnexpectedTypeException($value, 'integer|string');
        }

        if (!SiretService::isValidSiret($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
